ARG APP_NAME=vc-issuer
ARG APP_PATH=/app/$APP_NAME
ARG MODULE_NAME=vc_issuer
ARG REGISTRY=public.ecr.aws/docker/library
ARG PYTHON_VERSION=3.11.8
ARG POETRY_VERSION=1.7.1
ARG CI_PROJECT_URL
ARG FLASK_PORT=5001
ARG FLASK_HOST=0.0.0.0

###
# Stage: staging
###
FROM  $REGISTRY/python:$PYTHON_VERSION-slim AS staging
ARG APP_NAME
ARG APP_PATH
ARG POETRY_VERSION
ARG MODULE_NAME

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    POETRY_VERSION=${POETRY_VERSION} \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --no-install-recommends -y build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir "poetry==${POETRY_VERSION}"

# Import our project files
WORKDIR ${APP_PATH}
COPY ./poetry.lock ./pyproject.toml ./README.md ./
COPY ./${MODULE_NAME} ./

###
# Stage: build
###
FROM staging AS build
ARG APP_PATH
ARG APP_NAME

WORKDIR ${APP_PATH}
RUN poetry export --format requirements.txt --output requirements.txt --without-hashes

###
# Stage: production
###
FROM  $REGISTRY/python:$PYTHON_VERSION-alpine AS production
ARG APP_NAME
ARG APP_PATH
ARG CI_PROJECT_URL
ARG FLASK_PORT
ARG FLASK_HOST

LABEL name=$APP_NAME \
    description="A web server to sign vc or vp." \
    url=$CI_PROJECT_URL                 \
    maintainer="gxfs-fr"

ENV \
    USER_ID=65535 \
    GROUP_ID=65535 \
    USER_NAME=default-user \
    GROUP_NAME=default-user \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    FLASK_PORT=${FLASK_PORT} \
    FLASK_HOST=${FLASK_HOST}


RUN addgroup --gid ${GROUP_ID} ${GROUP_NAME} && \
    adduser --shell /sbin/nologin --disabled-password \
    --no-create-home --uid ${USER_ID} --ingroup ${GROUP_NAME} ${USER_NAME}

# hadolint ignore=DL3018
RUN apk --no-cache add curl

# Get build artifact wheel and install it respecting dependency versions
WORKDIR ${APP_PATH}
COPY --from=build ${APP_PATH}/ ${APP_PATH}/
COPY --from=build ${APP_PATH}/requirements.txt ${APP_PATH}/

RUN chown ${USER_NAME}:${GROUP_NAME} ${APP_PATH}/ 

# hadolint ignore=SC2086
RUN pip install --no-cache-dir --upgrade pip==23.3.2 && \
    pip install --no-cache-dir --requirement requirements.txt

EXPOSE ${FLASK_PORT}

ENTRYPOINT []
CMD ["python", "app.py"]

USER ${USER_NAME}
