# VC Issuer
## Gaia-X context
Gaia-X Credential documents are Verifiable Credential following the [W3C Verifiable Credentials Data Model 2.0](https://www.w3.org/TR/vc-data-model-2.0/) which can be presented in Verifiable Presentation. 

Gaia-X uses the [JSON-LD](https://www.w3.org/TR/json-ld11/) serialization of Verifiable Credentials with linked data proof format as stated in [Verifiable Credential Data Integrity](https://www.w3.org/TR/vc-data-model-2.0/#bib-vc-data-integrity). To ensure a Gaia-X Credential’s integrity and authenticity, claims must be cryptographically signed by the Participant that is issuing them.

Gaia-X uses the [JSON Web Signature Suite](https://w3c-ccg.github.io/lds-jws2020/#json-web-key-2020) for the Linked Data Proof of the Gaia-X Credentials. The Signature Suite utilizes Detached JWS signatures to provide support for a subset of the digital signature algorithms registered with IANA.

## VC-Issuer
*vc-issuer* is a web application designed for performing these signatures of Gaia-X credentials !

*vc-issuer* support Credential and Presentation signature, that is to say : 
- a credential signed will be a Verifiable Credential,
- a presentation signed will be a Verifiable Presentation.

*vc-issuer* can also generate VP Token according to OID4VP protocol.

For this reason, the term *credential* or *document* will be used to reference a credential/presentation.

## VC-Issuer Routes
Current version of *vc-issuer* which allows :
 - Hello World message : `HTTP GET` on `/`
 - Status of the server : `HTTP GET` on `/api/status`
 - Verifiable Credential signature : `HTTP PUT` on `/api/sign`
 - Verifiable Presentation signature : `HTTP PUT` on `/api/sign`
 - Credential Normalization : `HTTP PUT` on `/api/normalize`

## Supported signatures algorithms
It supports severals JSON Web Signature and Encryption Algorithms [see IANA Specification ([Signature encryption algorithms](https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms)) :
- **oct** (Octet Sequence)
  - HS256
  - HS384
  - HS512
- **RSA** (RSA Key Pairs) :
  - RS256
  - RS384
  - RS512
  - PS256
  - PS384
  - PS512
- **EC** (Elliptic Curve) :
  - ES256
  - ES384
  - ES512
  - ES256K
- **OKP** (Octet string key pairs) :
  - EdDSA

> Even if *vc-issuer* supports severals algorithms, only algorithms compatibles with related initialized key are permits ! For instance, if *vc-issuer* is started with RSA key, only algorithms related to RSA will be supported...

## Supported lifecycle management mechanisms
It supports severals lifecycle management mechanisms with status list :
- STATUS_LIST_2021: [**Verifiable Credentials Status List v2021**](https://www.w3.org/TR/2023/WD-vc-status-list-20230427/)
  - Outdated Privacy-preserving status information for Verifiable Credentials.
  - See implementation : [Revocation-Registry](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry)
- BITSTRING_STATUS_LIST: [**Bitstring Status List v1.0**](https://www.w3.org/TR/vc-bitstring-status-list/)
  - New Privacy-preserving status information for Verifiable Credentials.
  - See implementation : [Revocation-Registry-Bitstring](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry-bitstring)


# HowTo
## Step 1. Configure the cryptographic material

For instance, you can find a RSA 2048 bits example and development key (PEM format) in the *vc_issuer/config* directory.
Don't use this key in production ! 

```bash
cat vc_issuer/config/key_rsa_2048.priv

```

**PEM example of a key**

```bash
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAwpw0h8azKwjkSaJraUaqLBeRzrt9vpZChLAA2fPz8z7qpr90
...
zZvXdGJiZY0bd+CAmj6T/ehl7YXuE/UvY2zuqctZ7P+F+orY9+zeKQ==
-----END RSA PRIVATE KEY-----
```

## Step 2. Configure the DID documented

Have a look to our [user-agent](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent) repository, which do so.
It creates a DID and exposed it on a web-server.

## Step 3. Sign credential

### 3.1 Access the service in demo environment

*vc-issuer* is an internal service of a participant. It must not be exposed.

Use the following docker commands to test the service on local machine at http://localhost:5001:

```bash
docker compose build
docker compose up
```

You can also use the following command to connect to your service with a port-forwarding:

```bash
# Each participant is deployed inside a kubernetes namespace.
# For another particiapnt vc-issuer, replace the value abc-federation with the participant name (ex: auditor)
kubectl --kubeconfig kubeconfig.yaml --namespace abc-federation port-forward service/vc-issuer 5001:http
```

You can now reach the *vc-issuer* to http://localhost:5001.

### 3.2 Look at the credential to be signed

Go to *datas* directory

```bash
cd datas
cat vc_lambda.json
```

**Example Credential to be signed**

```json
{
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials", 
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@type": [
      "VerifiableCredential"
  ],
  "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/4cef2cda269bf8e1386f8dfa907f48f99e84df01fbaf7d764db586bf746ec8fd/data.json",
  "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
  "credentialSubject": {
      "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/9b29f510ce20317bd550c537c5e06cd87644403b619fab1f2c7c1b493b2c2d2f/data.json",
      "type": "gx:ServiceOffering",
      "gx:providedBy": {
          "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legalperson/data.json"
      },
      "gx:termsAndConditions": {
          "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/participant/eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/gaiax-terms-and-conditions.json",
          "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
      },
      "gx:name": "test",
      "aster-conformity:layer": "IAAS",
      "gx:keyword": [
          "Authentication"
      ],
      "gx:description": "dsqd",
      "gx:descriptionMarkDown": "",
      "gx:webAddress": "",
      "gx:dataProtectionRegime": [
          "GDPR2016"
      ],
      "gx:dataAccountExport": [
          {
              "gx:requestType": "email",
              "gx:accessType": "digital",
              "gx:formatType": "mime/png"
          }
      ],
      "gx:dependsOn": "",
      "gx:isAvailableOn": [
          ""
      ]
  }
}
```

#### 3.3 Submit the credential to the service

Submit your credential to the *vc-issuer* to get it signed.

#### 3.3.1 Signature type

**vc-issuer** supports multiple signature algorithms, you can specify algorithm as argument to the request using the keyword `algo_type`.

> *PS256* algorithm is the default algorithm used.

If you want to sign using another algorithm, your need to specify it. For instance, *RS256* algorithm :
```bash
curl -X PUT -H "Content-Type: application/json" -d '@data.json' "http://localhost:5001/api/sign?algo_type=RS256"

```

> Remember if *vc-issuer* is bootstraped with EC Key, only EC algorithms will be supported !

#### 3.3.2 StatusList type

**vc-issuer** supports several status list implementations to manage lifecycle of verifiable credential, you can specify it as argument to the request using the keyword `status_list_type` and parameters :
- BITSTRING_STATUS_LIST
- STATUS_LIST_2021
- NO_STATUS_LIST (by default)

> By default there is no status list used, that is to say no credentialStatus tag will be created in the Verifiable Credential forged.

If you want to sign using one, your need to specify it. For instance, *STATUS_LIST_2021* mechanism :
```bash
curl -X PUT -H "Content-Type: application/json" -d '@data.json' "http://localhost:5001/api/sign?status_list_type=STATUS_LIST_2021"
```


### 3.4 Get the credential signed as json result (or error messages)

**Example Verifiable Credential signed**

```json
{
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@type": [
    "VerifiableCredential"
  ],
  "@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/4cef2cda269bf8e1386f8dfa907f48f99e84df01fbaf7d764db586bf746ec8fd/data.json",
  "issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",
  "credentialSubject": {
    "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/9b29f510ce20317bd550c537c5e06cd87644403b619fab1f2c7c1b493b2c2d2f/data.json",
    "type": "gx:ServiceOffering",
    "gx:providedBy": {
      "id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/legalperson/data.json"
    },
    "gx:termsAndConditions": {
      "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/participant/eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/gaiax-terms-and-conditions.json",
      "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
    },
    "gx:name": "test",
    "aster-conformity:layer": "IAAS",
    "gx:keyword": [
      "Authentication"
    ],
    "gx:description": "dsqd",
    "gx:descriptionMarkDown": "",
    "gx:webAddress": "",
    "gx:dataProtectionRegime": [
      "GDPR2016"
    ],
    "gx:dataAccountExport": [
      {
        "gx:requestType": "email",
        "gx:accessType": "digital",
        "gx:formatType": "mime/png"
      }
    ],
    "gx:dependsOn": "",
    "gx:isAvailableOn": [
      ""
    ]
  },
  "expirationDate": "2024-08-20T01:51:08.344126+00:00",
  "issuanceDate": "2024-02-22T01:51:08.344126+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr",
    "created": "2024-02-22T01:51:08.344126+00:00",
    "jws": "eyJhbGciOiJFUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..EZkKU3FBMJjjkmtcicFJki4A10IJXJdoBOyktCHFA_eGO8iHbA6rQs0p6ihLLqwbinRQXLGiO3RjviGeqQsIYw"
  }
}
```

#### 3.4 Get the VC/VP signed as json result or error messages
To get more informations about *vc-issuer* configuraton and state :

```bash
curl -X GET "http://localhost:5001/api/status"
```

For instance,
```json
{
  "pk_loaded": true,
  "pk_path": "/app/creds/tls.key",
  "log_file": "/tmp/vcissuer.log",
  "key_type": "RSA",
  "key_curve": null,
  "algo_type": "PS256",
  "algos_supported": {
    "oct": [
      "HS256",
      "HS384",
      "HS512"
    ],
    "RSA": [
      "RS256",
      "RS384",
      "RS512",
      "PS256",
      "PS384",
      "PS512"
    ],
    "EC": [
      "ES256",
      "ES384",
      "ES512",
      "ES256K"
    ],
    "OKP": [
      "EdDSA"
    ]
  },
  "did_web_issuer": "did:web:abc-federation.gaia-x.community",
  "protected_proof_options": true,
  "detached_header": true,
  "protected_header": true
}
```

## Contributing

See the [Contributing](./CONTRIBUTING.md) file.
