## [1.0.2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/compare/1.0.1...1.0.2) (2024-2-29)


### Bug Fixes

* use public ECR docker registry instead of docker hub ([2b8bf12](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/commit/2b8bf12aa9fa74769a72460b00d7b7e255449239))

## [1.0.1](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/compare/1.0.0...1.0.1) (2024-2-29)


### Bug Fixes

* update readme ([76bbf79](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/commit/76bbf792bac2a51a9ea3722ff3d0b407265ad08a))
* wrong type when parsing environment variable EXPIRATION_DAYS ([5ad72e6](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/commit/5ad72e601654b5cd826c42c164645be40a263cce))
