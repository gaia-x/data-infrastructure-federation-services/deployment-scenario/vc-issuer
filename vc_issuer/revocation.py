import config
import logging, ssl, httpx

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

class StatusList2021():
    def __init__(self):
        self.revocation_url = config.REVOCATION_URL
        self.list_purpose = "revocation"
        self.issuer_id = config.ISSUER_NAME
        self.credential_url = f"{self.revocation_url}/api/v1/revocations/credentials"
        self.entry_url = f"{self.revocation_url}/api/v1/revocations/statusEntry?issuerId={self.issuer_id}"
        self.headers = {
            "Content-Type": "application/json"
        }

    def get_credential_status(self): 
        with httpx.Client() as client:
            try:
                logger.debug(f"Sending request to {self.entry_url}")

                response = client.post(
                    url=self.entry_url,
                    json={
                        "credentialUrl":self.credential_url,
                        "purpose":self.list_purpose
                    },
                    timeout=60.0,
                    headers=self.headers
                )

                logger.debug(f"Response status code: {response.status_code}")
                logger.debug(f"Response content: {response.content}")

                if response.status_code in (200, 201) and response.json():
                    return response.json()
                else:
                    raise Exception(response.text)
            except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
                raise Exception(f"{config.REVOCATION_URL} is not reachable") from err
            
class StatusListBitstring():
    def __init__(self):
        self.revocation_url = config.REVOCATION_BITSTRING_URL
        self.list_purpose = "message"
        self.type = "BIT_STRING"
        self.issuer_id = config.ISSUER_NAME
        self.entry_url = f"{self.revocation_url}/api/v1/revocations/statusEntry?type={self.type}&issuerId={self.issuer_id}"
        self.configure_url = f"{self.revocation_url}/api/v1/revocations/configure?type={self.type}&issuerId={self.issuer_id}"
        self.check_configuration_url = f"{self.configure_url}&purpose={self.list_purpose}"
        self.headers = {
            "X-API-KEY":config.REVOCATION_API_KEY,
            "Content-Type": "application/json"
        }
        if not self._check_configuration():
            self._configure_status_list()

    def _check_configuration(self): 
        with httpx.Client() as client:
            logger.debug("Checking if the statusList has been initiated")
            try:
                logger.debug(f"Sending request to {self.check_configuration_url}")

                response = client.get(
                    url=self.check_configuration_url,
                    timeout= 60.0,
                    headers=self.headers
                )

                logger.debug(f"Response status code => {response.status_code}")
                logger.debug(f"Response content => {response.content}")

                if response.status_code == 200:
                    return True
                elif response.status_code == 404:
                    logger.debug(f"StatusList has not been configured, returning False")
                    return False
                else:
                    raise Exception(response.text)
            except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
                raise Exception(f"{self.revocation_url} is not reachable") from err
            
    def _configure_status_list(self): 
        with httpx.Client() as client:
            logger.debug("Configuring revocation list")
            try:
                logger.debug(f"Sending request to {self.configure_url}")
                response = client.post(
                    url=self.configure_url,
                    json={
                        "purpose": self.list_purpose,
                        "reference": "",
                        "ttl": 100000,
                        "statusMessages": [
                            {
                                "status": "0x00",
                                "value": "VALID"
                            },
                            {
                                "status": "0x01",
                                "value": "SUSPENDED"
                            },
                            {
                                "status": "0x10",
                                "value": "REVOKED"
                            },
                            {
                                "status": "0x11",
                                "value": "EXPIRED"
                            }
                        ],
                        "size": 2,
                        "reversible": True
                    },
                    timeout= 60.0,
                    headers=self.headers
                    )
                logger.debug(f"Response status code => {response.status_code}")
                logger.debug(f"Response content => {response.content}")
                if response.status_code == 200:
                    return
                else:
                    raise Exception(response.text)
            except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
                raise Exception(f"{config.REVOCATION_URL} is not reachable") from err

    def get_credential_status(self): 
        with httpx.Client() as client:
            try:
                logger.debug(f"Sending request to {self.entry_url}")

                response = client.post(
                    url=self.entry_url,
                    json={
                    "purpose":self.list_purpose
                    },
                    timeout= 60.0,
                    headers=self.headers
                )

                logger.debug(f"Response status code: {response.status_code}")
                logger.debug(f"Response content: {response.content}")

                if response.status_code == 200 and response.json():
                    return response.json()[0]
                else:
                    raise Exception(response.text)
            except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
                raise Exception(f"{config.REVOCATION_URL} is not reachable") from err

class StatusListProvider:
    def __init__(self, status_list_type):
        self.status_list_type = status_list_type

    def get_credential_status(self):
        if self.status_list_type == config.STATUS_LIST_BITSTRING:
            return StatusListBitstring().get_credential_status()
        elif self.status_list_type == config.STATUS_LIST_2021:
            return StatusList2021().get_credential_status()
        else:
            return None
