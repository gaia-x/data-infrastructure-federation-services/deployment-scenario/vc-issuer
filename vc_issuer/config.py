import os
from typing import Final
from dotenv import load_dotenv

load_dotenv()

ROOT_DIR: Final = os.path.realpath(os.path.dirname(__file__))

# Flask config
FLASK_PORT = os.getenv('FLASK_PORT', 5001)
FLASK_HOST = os.getenv('FLASK_HOST', "0.0.0.0")

# Log config
LOG_FILE_NAME = os.getenv('LOG_FILE_NAME', "/tmp/vcissuer.log")

# Issuer config
PRIVATE_KEY_PATH = os.getenv('PRIVATE_KEY_PATH', f"{ROOT_DIR}/config/key_rsa_2048.priv")
DEFAULT_SIGNATURE_ALG = os.getenv('DEFAULT_SIGNATURE_ALGORITHM', "PS256")
ISSUER_NAME= os.getenv('ISSUER_NAME', "IssuerName")
DID_WEB_ISSUER = os.getenv('DID_WEB_ISSUER', "did:web:agdatahub.provider.demo23.gxfs.fr")

# Revocation config
REVOCATION_URL= os.getenv('REVOCATION_URL', "https://revocation-registry.abc-federation.dev.gaiax.ovh")
#REVOCATION_URL= os.getenv('REVOCATION_URL', "https://revocation-registry.aster-x.demo23.gxfs.fr")
REVOCATION_API_KEY = os.getenv('REVOCATION_API_KEY', "foobar")

# Revocation bitstring config
REVOCATION_BITSTRING_URL = os.getenv('REVOCATION_BITSTRING_URL', "https://revocation-registry-bitstring.abc-federation.dev.gaiax.ovh")
#REVOCATION_BITSTRING_URL= os.getenv('REVOCATION_BITSTRING_URL', "https://revocation-registry-bitstring.aster-x.demo23.gxfs.fr")
#TODO: Change key with new one
REVOCATION_BITSTRING_API_KEY = REVOCATION_API_KEY

# Credential config
EXPIRATION_DAYS = int(os.getenv('EXPIRATION_DAYS', "180"))
CREDENTIAL_CONTEXT = os.getenv('CREDENTIAL_CONTEXT', "https://www.w3.org/2018/credentials/v1") 
#CREDENTIAL_CONTEXT = os.getenv('CREDENTIAL_CONTEXT', "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials")
SUITES_JWS_CONTEXT = os.getenv('SUITES_JWS_CONTEXT', "https://w3id.org/security/suites/jws-2020/v1")
#SUITES_JWS_CONTEXT = os.getenv('SUITES_JWS_CONTEXT', "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020")
VC_CONTEXT = [
    CREDENTIAL_CONTEXT,
    SUITES_JWS_CONTEXT,
]

# JWS config
PROTECTED_PROOF_OPTIONS = True
DETACHED_PAYLOAD = True
PROTECTED_HEADER = True

# HTTP ARGS
ARG_ALG_TYPE:Final = "algo_type"
ARG_STATUS_LIST_TYPE: Final = "status_list_type"
ARG_CHALLENGE: Final = "challenge"
ARG_DOMAIN: Final = "domain"

# Keys
KTY_OCT:Final = "oct" #Octet sequence (symmetric)
KTY_RSA:Final = "RSA" #RSA key pairs
KTY_EC:Final  = "EC"  #Elliptic Curve
KTY_OKP:Final = "OKP" #Octet string key pairs
SUPPORTED_KEY_TYPES:Final = [KTY_OCT, KTY_RSA, KTY_EC, KTY_OKP]
KTY_WITH_CRV:Final = [KTY_EC, KTY_OKP]

# Signature algorithms (cf. https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms)
ALG_HS256:Final  = "HS256"
ALG_HS384:Final  = "HS384"
ALG_HS512:Final  = "HS512"
ALG_RS256:Final  = "RS256"
ALG_RS384:Final  = "RS384"
ALG_RS512:Final  = "RS512"
ALG_PS256:Final  = "PS256"
ALG_PS384:Final  = "PS384"
ALG_PS512:Final  = "PS512"
ALG_ES256:Final  = "ES256"
ALG_ES384:Final  = "ES384"
ALG_ES512:Final  = "ES512"
ALG_ES256K:Final = "ES256K"
ALG_EDDSA:Final  = "EdDSA"

#Curves (cf. https://www.iana.org/assignments/jose/jose.xhtml#web-key-elliptic-curve)
CRV_P256:Final      = "P-256"
CRV_P384:Final      = "P-384"
CRV_P521:Final      = "P-521"
CRV_SECP256K1:Final = "secp256k1"
CRV_ED25519:Final   = "Ed25519"
CRV_X25519:Final    = "X25519"
CRV_ED448:Final     = "Ed448"
CRV_X448:Final      = "X448"

# Signatures
SUPPORTED_OCT_SIGNATURE_ALGS:Final = [ALG_HS256, ALG_HS384, ALG_HS512]
SUPPORTED_RSA_SIGNATURE_ALGS:Final = [ALG_RS256, ALG_RS384, ALG_RS512, ALG_PS256, ALG_PS384, ALG_PS512]
SUPPORTED_EC_SIGNATURE_ALGS:Final  = [ALG_ES256, ALG_ES384, ALG_ES512, ALG_ES256K]
SUPPORTED_OKP_SIGNATURE_ALGS:Final = [ALG_EDDSA]
SUPPORTED_SIGNATURE_ALGS:Final = [*SUPPORTED_OCT_SIGNATURE_ALGS, *SUPPORTED_RSA_SIGNATURE_ALGS, 
                   *SUPPORTED_EC_SIGNATURE_ALGS, *SUPPORTED_OKP_SIGNATURE_ALGS]
SUPPORTED_ALGS_BY_KTY_DICT:Final = {
    KTY_OCT: SUPPORTED_OCT_SIGNATURE_ALGS,
    KTY_RSA: SUPPORTED_RSA_SIGNATURE_ALGS, 
    KTY_EC: SUPPORTED_EC_SIGNATURE_ALGS,
    KTY_OKP: SUPPORTED_OKP_SIGNATURE_ALGS
}
SUPPORTED_CURVES_BY_KTY_DICT:Final = {
    KTY_EC: [CRV_P256, CRV_P384, CRV_P521, CRV_SECP256K1],
    KTY_OKP: [CRV_ED25519, CRV_X25519, CRV_ED448, CRV_X448]
}
SUPPORTED_ALGS_BY_CRV_DICT:Final = {
    CRV_P256: [ALG_ES256],
    CRV_P384: [ALG_ES384],
    CRV_P521: [ALG_ES512],
    CRV_SECP256K1: [ALG_ES256K],
    CRV_ED25519: [ALG_EDDSA],
    CRV_X25519: [ALG_EDDSA],
    CRV_ED448: [ALG_EDDSA],
    CRV_X448: [ALG_EDDSA]
}

STATUS_LIST_NO: Final        = "NO_STATUS_LIST"
STATUS_LIST_2021: Final      = "STATUS_LIST_2021"
STATUS_LIST_BITSTRING: Final = "BITSTRING_STATUS_LIST"
SUPPORTED_STATUS_LISTS: Final = [
   STATUS_LIST_NO, STATUS_LIST_2021, STATUS_LIST_BITSTRING 
]
DEFAULT_STATUS_LIST: Final = STATUS_LIST_NO