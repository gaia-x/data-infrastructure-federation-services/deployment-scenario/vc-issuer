from jwcrypto.jws import JWK
from typing import Optional
import config

def string_to_bytes(str_data: str) -> bytes:
    """
    Convert string to bytes
    """
    if not isinstance(str_data, str):
        raise ValueError("Data is not string")
    return str_data.encode('utf-8')

def bytes_to_string(bytes_data: bytes) -> str:
    """
    Convert bytes to string
    """
    if not isinstance(bytes_data, bytes):
        raise ValueError("Data is not bytes")
    return bytes_data.decode('utf-8')

def check_kty_alg_crv(key: JWK, alg: str) -> bool:
    """
    Check compatibility between key type, key curve, and signature algorithm required.
    """
    kty = key.key_type

    if not kty or kty not in config.SUPPORTED_KEY_TYPES:
        err = f"Key type ({kty}) is not supported. (Supported: {config.SUPPORTED_KEY_TYPES})"
        raise Exception(err)
    if alg not in config.SUPPORTED_SIGNATURE_ALGS:
        err = f"Algo type ({alg}) is not supported. (Algo supported: {config.SUPPORTED_SIGNATURE_ALGS})"
        raise Exception(err)
    if alg not in config.SUPPORTED_ALGS_BY_KTY_DICT.get(kty):
        err = f"Algo type ({alg}) is not supported for key type ({kty}). (Algo supported: {config.SUPPORTED_ALGS_BY_KTY_DICT.get(kty)})"
        raise Exception(err)
    if kty in config.KTY_WITH_CRV:
        crv = key.key_curve
        if alg not in config.SUPPORTED_ALGS_BY_CRV_DICT.get(crv):
            err = f"Curve ({crv}) is not supported for alg type ({alg}). (Curves supported: {config.SUPPORTED_ALGS_BY_CRV_DICT.get(crv)})"
            raise Exception(err)
    return True

def get_curve(key: JWK) -> Optional[str]:
    """
    Return curve of key if exists
    """
    kty = key.key_type

    if kty in config.KTY_WITH_CRV:
        return key.key_curve
    else:
        return None
    

def check_algo_type(algo_type):
    if algo_type not in config.SUPPORTED_SIGNATURE_ALGS:
        err = f"Algo type ({algo_type}) is not supported. (Supported: {config.SUPPORTED_SIGNATURE_ALGS})"
        raise Exception(err)
    return True

def check_status_list_type(status_list_type):
    if status_list_type not in config.SUPPORTED_STATUS_LISTS:
        err = f"StatusList type ({status_list_type}) is not supported. (Supported: {config.SUPPORTED_STATUS_LISTS})"
        raise Exception(err)
    return True