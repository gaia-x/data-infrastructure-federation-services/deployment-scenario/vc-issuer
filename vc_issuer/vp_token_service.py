import logging
import pytz
import uuid
from dataclasses import dataclass, field
from datetime import datetime, timedelta

from dataclasses_json import config, dataclass_json
from jose import jwt
from jwcrypto.jwk import JWK

import config as aster_config

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


@dataclass_json
@dataclass
class VerifiablePresentation:
    context: set = field(metadata=config(field_name="@context"))
    type: set = field(metadata=config(field_name="@type"))
    verifiableCredential: set


class VPTokenService:
    def __init__(self):
        self.revocation_url = aster_config.REVOCATION_URL
        self.list_purpose = "revocation"
        self.issuer_id = aster_config.ISSUER_NAME
        self.credential_url = f"{self.revocation_url}/api/v1/revocations/credentials"
        self.entry_url = f"{self.revocation_url}/api/v1/revocations/statusEntry?issuerId={self.issuer_id}"
        self.headers = {
            "Content-Type": "application/json"
        }

    def issue_vp_token(self, issuer, subject, vcs, key: JWK) -> str:
        logger.debug(f"Issue VP token for issuer[{issuer}], subject[{subject}] and vcs[{vcs}]")
        logger.debug(f"Id of key: {key.key_id}")

        m_vp = VerifiablePresentation(context=["https://www.w3.org/2018/credentials/v1"],
                                      type=["VerifiablePresentation"],
                                      verifiableCredential=vcs)

        days_for_expiration = aster_config.EXPIRATION_DAYS
        current_time = datetime.now(pytz.utc)
        my_time = current_time.isoformat()
        expiration_date = (current_time + timedelta(days=days_for_expiration)).isoformat()

        claims = {
            "iss": issuer,
            "sub": subject,
            "iat": my_time,
            "exp": expiration_date,
            "jti": str(uuid.uuid4()),
            "nbf": datetime.now(pytz.utc).isoformat(),
            "custom_claim": "API-GateWay",
            "vp": m_vp.to_dict()
        }

        headers = {
            "kid": str(key.key_id),
            "typ": "JWT",
            "alg": "RS256"
        }

        jwt_encoded = jwt.encode(claims, key, headers=headers, algorithm='RS256')

        logger.debug(f"JWT result: [{jwt_encoded}]")

        return jwt_encoded
