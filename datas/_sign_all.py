import requests, os, json

headers = {
    "Content-Type": "application/json; charset=UTF-8",
    "Accept": "application/json, application/javascript, text/javascript, text/json"
}
url = "http://localhost:5001/api/v0.9/sign"
n, succ, err = 0, 0, 0

for obj in os.listdir():
    if ".json" in obj:
        n += 1
        with open(obj, 'r') as f:
            try:
                js = json.load(f)
                res = requests.put(url=url, json=js, headers=headers)
                if res.status_code == 200:
                    succ += 1
                    print(f"Success with {obj}")
                else:
                    err += 1
                    print(f"Error with {obj}")
            except Exception as e:
                err += 1
                print(f"Error with {obj}")

print(f"\nRecap: \n{n} tests: \n\t{succ} success \n\t{err} error")