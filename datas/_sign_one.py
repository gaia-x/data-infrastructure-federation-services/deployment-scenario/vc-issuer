import requests, os, json

headers = {
    "Content-Type": "application/json; charset=UTF-8",
    "Accept": "application/json, application/javascript, text/javascript, text/json"
}
url = "http://localhost:5001/api/v0.9/sign?algotype=ES512"
n, succ, err = 0, 0, 0

filename = "vc_lambda"

with open(f"{filename}.json", 'r') as f:
    try:
        js = json.load(f)
        res = requests.put(url=url, json=js, headers=headers)
        if res.status_code == 200:
            with open(f"{filename}_signed.json", 'wb') as ff:
                ff.write(res.content)
        print(res.content)
    except Exception as e:
        print(f"{e}")