vc_agdatahub_to_sign = {
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering/dfffaf01-47ee-42da-986d-638c24fa4987/data.json",
  "@type": [
    "VerifiableCredential",
    "ServiceOffering"
  ],
  "credentialSubject": {
    "id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering-json/c7a47a1a-dedf-4af5-8390-c079fb5a299a/data.json",
    "type": "gx:ServiceOffering",
    "aster-conformity:layer": "IAAS",
    "gx:dataAccountExport": [
      {
        "gx:accessType": "digital",
        "gx:formatType": "mime/png",
        "gx:requestType": "email"
      }
    ],
    "gx:dataProtectionRegime": [
      "GDPR2016"
    ],
    "gx:dependsOn": "",
    "gx:description": "Lorem Ipsum",
    "gx:descriptionMarkDown": "",
    "gx:isAvailableOn": [
      ""
    ],
    "gx:keyword": [
      "Authentication"
    ],
    "gx:name": "Lorem Ipsum Service",
    "gx:providedBy": {
      "id": "https://agdatahub.provider.demo23.gxfs.fr/legal-participant/20ba28ec-418d-4f97-b14e-16d781c1461b/data.json"
    },
    "gx:termsAndConditions": {
      "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/gaiax-terms-and-conditions/a96d2470-62e7-426a-88fd-64dbc311cfb6/data.json",
      "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
    },
    "gx:webAddress": ""
  }
}

vc_agdatahub_signed_with_status_list_2021 = {
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering/dfffaf01-47ee-42da-986d-638c24fa4987/data.json",
  "@type": [
    "VerifiableCredential",
    "ServiceOffering"
  ],
  "credentialSubject": {
    "aster-conformity:layer": "IAAS",
    "gx:dataAccountExport": [
      {
        "gx:accessType": "digital",
        "gx:formatType": "mime/png",
        "gx:requestType": "email"
      }
    ],
    "gx:dataProtectionRegime": [
      "GDPR2016"
    ],
    "gx:dependsOn": "",
    "gx:description": "Lorem Ipsum",
    "gx:descriptionMarkDown": "",
    "gx:isAvailableOn": [
      ""
    ],
    "gx:keyword": [
      "Authentication"
    ],
    "gx:name": "Lorem Ipsum Service",
    "gx:providedBy": {
      "id": "https://agdatahub.provider.demo23.gxfs.fr/legal-participant/20ba28ec-418d-4f97-b14e-16d781c1461b/data.json"
    },
    "gx:termsAndConditions": {
      "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/gaiax-terms-and-conditions/a96d2470-62e7-426a-88fd-64dbc311cfb6/data.json",
      "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
    },
    "gx:webAddress": "",
    "id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering-json/c7a47a1a-dedf-4af5-8390-c079fb5a299a/data.json",
    "type": "gx:ServiceOffering"
  },
  "credentialStatus": {
    "type": "StatusList2021Entry",
    "id": "https://revocation-registry.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/agdatahub-revocation#16",
    "statusPurpose": "revocation",
    "statusListIndex": "16",
    "statusListCredential": "https://revocation-registry.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/agdatahub-revocation"
  },
  "expirationDate": "2024-11-11T12:54:12.672497+00:00",
  "issuanceDate": "2024-05-15T12:54:12.672497+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr",
    "created": "2024-05-15T12:54:12.672497+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..CdDie2CwNaROZMPksjLEYNGV5CEkiu0VBny9DVLgAY95RM9aGwHas69vlJ0XmYhYRXZ7mgorLP8ZBQghfRyIh1tIRstvrcK4Y5_McMhu0rjnsMwvj--9IldWcXuP4EBW65dgKINrlJ862T5SG-oygzlsDmFbBTy42kPYHLalg8OtoaMHrHAV1h1bo3jH_zNfzJmIYn6ahVsGeYFr2eTzHOmvJA6NUztTiULNFiGUXlOMpDOv4WCVVrZBlQpTmZH9piLom388c1c2W0glNlh8uSni1_u0V9xFRpleZzPSrJzHdm7h24IwrwiKsKR-7XwLVUPC1x-d8Zgoy3KHirIOhA"
  }
}

vc_agdatahub_signed_with_bitstring_status_list = {
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering/dfffaf01-47ee-42da-986d-638c24fa4987/data.json",
  "@type": [
    "VerifiableCredential",
    "ServiceOffering"
  ],
  "credentialSubject": {
    "aster-conformity:layer": "IAAS",
    "gx:dataAccountExport": [
      {
        "gx:accessType": "digital",
        "gx:formatType": "mime/png",
        "gx:requestType": "email"
      }
    ],
    "gx:dataProtectionRegime": [
      "GDPR2016"
    ],
    "gx:dependsOn": "",
    "gx:description": "Lorem Ipsum",
    "gx:descriptionMarkDown": "",
    "gx:isAvailableOn": [
      ""
    ],
    "gx:keyword": [
      "Authentication"
    ],
    "gx:name": "Lorem Ipsum Service",
    "gx:providedBy": {
      "id": "https://agdatahub.provider.demo23.gxfs.fr/legal-participant/20ba28ec-418d-4f97-b14e-16d781c1461b/data.json"
    },
    "gx:termsAndConditions": {
      "gx:URL": "https://agdatahub.provider.demo23.gxfs.fr/gaiax-terms-and-conditions/a96d2470-62e7-426a-88fd-64dbc311cfb6/data.json",
      "gx:hash": "a452d70abc5870d06cb9482c41d62c1fde03bc166ca6d5dddc9133358fd88866"
    },
    "gx:webAddress": "",
    "id": "https://agdatahub.provider.demo23.gxfs.fr/service-offering-json/c7a47a1a-dedf-4af5-8390-c079fb5a299a/data.json",
    "type": "gx:ServiceOffering"
  },
  "credentialStatus": {
    "type": "BitstringStatusListEntry",
    "id": "https://revocation-registry-bitstring.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/agdatahub-message-bit-string#14",
    "statusPurpose": "message",
    "statusListIndex": "14",
    "statusListCredential": "https://revocation-registry-bitstring.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/agdatahub-message-bit-string"
  },
  "expirationDate": "2024-11-11T12:57:04.320027+00:00",
  "issuanceDate": "2024-05-15T12:57:04.320027+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr",
    "created": "2024-05-15T12:57:04.320027+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..JaEli0VgO1fbL7TyUZk7878xA8stxgPwMb5SCbuBE44Uw5KwJszVKJRtT_6_92Xv_s-EcgSU5VXJN2SjHrQdmccwVrT5Ap9jZxsQiF-xC2Egyz930Ub7dm-ErWGQ1WA7FL0lAUje8V__4dHbix0xSNMVBHeuFDYXp-6IQLCuCtQ1BZrW-rvN0kH7KSAQ2d1Srhx3_Ohe2uWHfs37am1DKOCOo_xS2I9m_0B9bUASP597LhMOnV6S9Umlo4qzzTTfwpcbF716gzcUuFVoL9J4BDIEObDsP-0bERr3EZu-1HA39nBeX6orEsBdfM_2IY0qw0sClDRSdj5uas7pZD2tMQ"
  }
}

vp_dufourstorage = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "@type": [
        "VerifiablePresentation"
    ],
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1",
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            ],
            "type": [
                "VerifiableCredential"
            ],
            "id": "https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203",
            "issuer": "did:web:compliance.lab.gaia-x.eu:v1-staging",
            "issuanceDate": "2024-02-27T09:08:10.581Z",
            "expirationDate": "2024-05-27T09:08:10.581Z",
            "credentialSubject": [
                {
                    "type": "gx:compliance",
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649:vc:6f7a36cf-c755-48fa-a0a9-a0bce061bfd6:data.json",
                    "gx:integrity": "sha256-ac1473cd55926e12e9b02fedf0c672b03710ba3a0f8886452aeef6fe1a558ac8",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:GaiaXTermsAndConditions"
                },
                {
                    "type": "gx:compliance",
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider/data.json",
                    "gx:integrity": "sha256-3c51c808ec919916a0ba2455f1a4e02fed96c4b84d8f31d2c1796edfdda112e4",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:LegalParticipant"
                },
                {
                    "type": "gx:compliance",
                    "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5bssssse831bb07d4f260f/vc/c008c2c75f190cf27030dbb4ca06f36f4370d3ee0482812f3032a2bcd43b0d33/data.json",
                    "gx:integrity": "sha256-672ae8aef23549d069ee5757eec5f46a10433f1bcb2e218ee78c533234a98231",
                    "gx:integrityNormalization": "RFC8785:JCS",
                    "gx:version": "22.10",
                    "gx:type": "gx:legalRegistrationNumber"
                }
            ],
            "proof": {
                "type": "JsonWebSignature2020",
                "created": "2024-02-27T09:08:11.190Z",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:compliance.lab.gaia-x.eu:v1-staging#X509-JWK2020",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Fl42M5csoVh3HLfKELB_bY99MllgroS7uMIzzOm4FcdlxFKzemF4JOc94Bx7xIvMOQlXpw22gjnurycHkqHddUrv6fsiZDfsOqMOzf6TPiwJkUlD8F8x2bqmbFK0GXAFbZz_MBSb6tq85dpR0iS2TTGywj1-hT4qDLslbhe1z6Sk0uX2smu2HdV9qbGPGSWoLb25xDp4PNn7EJiXDiuvl6nnGWk2jr9U6a7AsI67TmNAiSk4EP6xilXe1rV8EnCri-G8DcpZym8jATcbjEf0umaMs-4p05q4AiQtDfoRZe3nd_gMwkIYkcnMDekN3mEekKnyqBXoNpY6ByJ2nPuNcQ"
            }
        },
        {
            "type": [
                "VerifiableCredential"
            ],
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1",
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            ],
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8b866561-2354-4ca4-b6d2-3fe5cd46a2b9/data.json",
            "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider/data.json",
                "aster-conformity:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n- to be and remain GaiaX compliant while being a member of this Federation\nThe Federation will revoke any Verifiable Credential emmitted by the Federation if the Federation becomes aware of any inaccurate statement in regards to the two rules listed",
                "type": "aster-conformity:TermsAndConditions"
            },
            "expirationDate": "2024-08-25T15:56:08.816891+00:00",
            "issuanceDate": "2024-02-27T15:56:08.816891+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "created": "2024-02-27T15:56:08.816891+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..AtjSFwqtgcXJzH-TcXN8JXKPbGq98QLQyyyZQRvDpsmEy23H9EYF7TjE8c8Uj3keQjbzyhImVzdQdIYrf_QO2NyXTvIOzAXwYpruW006U6iqjTuH7IbgCAE5wyHkOkqApe5pUVH2ngi2L-23J5ak8pRclQJCd-7-2xp2KwahszjJDtr-3S2rpLs44t9SEysXw3y_ONgVEuSGQyjACG2GLxZ27gFtd3EwAQW9HOMhKue1VGI8JcqQNS5eFP_cSaERV3LmT3O2UsEZvl199JTpUSVATsI30CdSYAqAMZCcEXGcaFh8v6qxnsjaLaHFVYryTDFt6Va388gIutyymJRc9w"
            }
        },
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://w3id.org/security/suites/jws-2020/v1",
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            ],
            "id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider/data.json",
            "type": [
                "VerifiableCredential"
            ],
            "issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
            "credentialSubject": {
                "type": "gx:LegalParticipant",
                "id": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "gx:headquarterAddress": {
                    "gx:addressCountryCode": "IT",
                    "gx:countrySubdivisionCode": "IT-25",
                    "gx:streetAddress": "Via Europa",
                    "gx:postalCode": "20121"
                },
                "gx:legalName": "Dufour Storage",
                "gx:legalAddress": {
                    "gx:addressCountryCode": "IT",
                    "gx:countrySubdivisionCode": "IT-25",
                    "gx:streetAddress": "Via Europa",
                    "gx:postalCode": "20121"
                },
                "gx:legalRegistrationNumber": {
                    "gx:vatID": "BE0762747721"
                },
                "aster-conformity:blockchainAddress": "0xaDlcpsod"
            },
            "expirationDate": "2024-08-25T08:57:44.652288+00:00",
            "issuanceDate": "2024-02-27T08:57:44.652288+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
                "created": "2024-02-27T08:57:44.652288+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ms9uF1pI60vTfSzBlV71jvn4qjUNNuMs_xJQCzqowaGfarTdJxrr1g6Ufzs9QxcKpNUUf07yUGcSTtSboEPWAKoChMVq9oUuzyahkvvECtjTSm3Lhuuim1MKRd-EMa_vzo0XS2hZtNmrRK9qm_wnGQHNViDYf7F6KtCrtL5Mx7-mgHl69WqI-MiRRRzOnjVtrauU3cVmIWY29gZDSrVq3bal-arbKDBaV4NB3Rowj1vlPduCO04pUgygD3xG1IWoQuxMqZ0mowlzCbLwIZzzS7G18FkJ4_P4DvbQ8fNu0AShYxKUzTBfP8Mp6hwpWArymcT-xBJcoXsW8Pn2dOtY9w"
            }
        }
    ]
}

