import json, respx, httpx
from jwcrypto import jwk, jws
from pyld import jsonld
from hashlib import sha256
from vc_issuer.app import app
from vc_issuer import config, utils
from tests import datas

class Utils:
    def sign(client, document, alg=config.DEFAULT_SIGNATURE_ALG, status_list=config.DEFAULT_STATUS_LIST, challenge="", domain=""):
        #Signature
        http_args = f"{config.ARG_ALG_TYPE}={alg}"
        http_args = http_args + f"&{config.ARG_STATUS_LIST_TYPE}={status_list}"
        http_args = http_args + f"&{config.ARG_CHALLENGE}={challenge}"
        http_args = http_args + f"&{config.ARG_DOMAIN}={domain}"

        response = client.put(f'/api/sign?{http_args}', json=document)

        return response

    def verify(document_signed):
        jws_value = document_signed.get("proof").get("jws")

        #Verification step 1 : get info from jws
        protected_b64, payload_b64, signature_b64 = jws_value.split('.')
        jws_dict = {
            "protected": protected_b64,
            "payload": payload_b64,
            "signature": signature_b64
        }
        jws_serialized = json.dumps(jws_dict)
        jwstoken = jws.JWS()
        jwstoken.deserialize(jws_serialized)

        #Verification, step 2 : get public key from private key
        with open(config.PRIVATE_KEY_PATH, "rb") as file:
            private_key = jwk.JWK.from_pem(file.read())
            public_key = private_key.public()

        #Verification, step 3 : recreate payload (detached payload is not included in jws)
        credential = document_signed.copy()
        del credential["proof"]
        credential_normalized = jsonld.normalize(credential, {"algorithm": "URDNA2015", "format": "application/n-quads"})
        # Recreate empty proof with context
        proof = document_signed["proof"].copy()
        proof["@context"] = document_signed.get('@context', config.VC_CONTEXT)
        del proof["jws"]
        proof_normalized = jsonld.normalize(proof, {"algorithm": "URDNA2015", "format": "application/n-quads"})
        proof_hashed = sha256(utils.string_to_bytes(proof_normalized)).digest()
        credential_hashed = sha256(utils.string_to_bytes(credential_normalized)).digest()
        payload_tosign = proof_hashed + credential_hashed

        try:
            jwstoken.verify(public_key, detached_payload=payload_tosign)
            return True
        except Exception as e:
            print(e)
        return False


class TestHelloWorld:
    def test_hello_world(self, client):
        response = client.get('/')

        assert response.status_code == 200
        assert b"Hello VC-Issuer!" in response.data


class TestSign:
    def test_sign_vc_ec(self, client):
        is_ec = config.KTY_EC.lower() in config.PRIVATE_KEY_PATH

        if is_ec:
            for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                if alg == config.DEFAULT_SIGNATURE_ALG:
                    assert response.status_code == 200
                    assert Utils.verify(response.json) == True
                else:
                    assert response.status_code == 400
        else:
            for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                assert response.status_code == 400
                
    def test_sign_vc_rsa(self, client):
        is_rsa = config.KTY_RSA.lower() in config.PRIVATE_KEY_PATH

        if is_rsa:
            for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                assert response.status_code == 200
                assert Utils.verify(response.json) == True
        else:
            for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                assert response.status_code == 400

    def test_sign_vc_okp(self, client):
        is_okp = config.KTY_OKP.lower() in config.PRIVATE_KEY_PATH

        if is_okp:
            for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                if alg == config.DEFAULT_SIGNATURE_ALG:
                    assert response.status_code == 200
                    assert Utils.verify(response.json) == True
                else:
                    print(response.json)
                    assert response.status_code == 400
        else:
            for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                assert response.status_code == 400

    def test_sign_vp_ec(self, client):
        is_ec = config.KTY_EC.lower() in config.PRIVATE_KEY_PATH

        if is_ec:
            for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                if alg == config.DEFAULT_SIGNATURE_ALG:
                    assert response.status_code == 200
                    assert Utils.verify(response.json) == True
                else:
                    assert response.status_code == 400
        else:
            for alg in config.SUPPORTED_EC_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, alg=alg)
                assert response.status_code == 400
                
    def test_sign_vp_rsa(self, client):
        is_rsa = config.KTY_RSA.lower() in config.PRIVATE_KEY_PATH

        if is_rsa:
            for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vp_dufourstorage, alg=alg)
                assert response.status_code == 200
                assert Utils.verify(response.json) == True
        else:
            for alg in config.SUPPORTED_RSA_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vp_dufourstorage, alg=alg)
                assert response.status_code == 400

    def test_sign_vp_okp(self, client):
        is_okp = config.KTY_OKP.lower() in config.PRIVATE_KEY_PATH

        if is_okp:
            for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vp_dufourstorage, alg=alg)
                if alg == config.DEFAULT_SIGNATURE_ALG:
                    assert response.status_code == 200
                    assert Utils.verify(response.json) == True
                else:
                    print(response.json)
                    assert response.status_code == 400
        else:
            for alg in config.SUPPORTED_OKP_SIGNATURE_ALGS:
                response = Utils.sign(client=client, document=datas.vp_dufourstorage, alg=alg)
                assert response.status_code == 400


class TestNormalize:
    def test_normalize(self, client):
        response = client.put('/api/normalize', json=datas.vc_agdatahub_to_sign)
        assert response.status_code == 200
        
        normalized_original = jsonld.normalize(datas.vc_agdatahub_to_sign, {"algorithm": "URDNA2015", "format": "application/n-quads"})
        hashed_normalized_original = sha256(utils.string_to_bytes(normalized_original)).digest()
        hashed_normalized_response = sha256(response.data).digest()

        assert hashed_normalized_original == hashed_normalized_response


class TestStatus:
    def test_status(self, client):
        response = client.get('/api/status')
        assert response.status_code == 200

    def test_status_v09(self, client):
        response = client.get('/api/v0.9/status')
        assert response.status_code == 200


class TestStatusList:
    def test_sign_vc_no_status_list(self, client):
        response = Utils.sign(client=client, document=datas.vp_dufourstorage, status_list=config.STATUS_LIST_NO)
        
        assert response.status_code == 200
        assert response.json.get('credentialStatus') is None
        assert Utils.verify(response.json) == True

    @respx.mock
    def test_sign_vc_status_list_2021_mock(self, client):
        #Mock revocation registry call (return a credentialStatus)
        endpoint = respx.post(url=config.REVOCATION_URL).respond(
            status_code=httpx.codes.OK, json=datas.vc_agdatahub_signed_with_status_list_2021.get('credentialStatus')
        )

        response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, status_list=config.STATUS_LIST_2021)
        
        assert endpoint.called
        assert response.status_code == httpx.codes.OK
        assert response.json is not None
        assert response.json.get('credentialStatus') is not None 
        assert response.json.get('credentialStatus').get('type') == "StatusList2021Entry"
        assert Utils.verify(response.json) == True

    @respx.mock
    def test_sign_vc_status_list_bitstring_mock(self, client):
        #Mock revocation registry conf (return true)
        endpoint_configure = respx.get(url=config.REVOCATION_BITSTRING_URL).respond(
            status_code=httpx.codes.OK, json={}
        )
        #Mock revocation registry call (return list of credentialStatus)
        endpoint_create = respx.post(url=config.REVOCATION_BITSTRING_URL).respond(
            status_code=httpx.codes.OK, json=[datas.vc_agdatahub_signed_with_bitstring_status_list.get('credentialStatus')]
        )
        response = Utils.sign(client=client, document=datas.vc_agdatahub_to_sign, status_list=config.STATUS_LIST_BITSTRING)
        
        print(f"Response JSON: {response.json}")
        assert endpoint_configure.called
        assert endpoint_create.called
        assert response.status_code == httpx.codes.OK
        assert response.json is not None
        assert response.json.get('credentialStatus') is not None 
        assert response.json.get('credentialStatus').get('type') == "BitstringStatusListEntry"
        assert Utils.verify(response.json) == True

    def test_sign_vp_challenge(self, client):
        challenge = "challenge_foobar"
        response = Utils.sign(client=client, document=datas.vp_dufourstorage, challenge=challenge)
        
        assert response.status_code == 200
        print(response.json)
        assert response.json.get('proof').get('challenge') == challenge

    def test_sign_vp_domain(self, client):
        domain = "domain_foobar"
        response = Utils.sign(client=client, document=datas.vp_dufourstorage, domain=domain)
        
        assert response.status_code == 200
        print(response.json)
        assert response.json.get('proof').get('domain') == domain